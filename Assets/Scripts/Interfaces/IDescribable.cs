public interface IDescribable {

  string Title       { get; }
  string Description { get; }

}
