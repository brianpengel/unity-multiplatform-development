using UnityEngine ;

[RequireComponent( typeof(LaserWeapon) )]
public class LaserWeaponAnimator : MonoBehaviour {

  [SerializeField] private Material material ;
  [SerializeField] private Color chargeColour = Color.red ;
  [SerializeField] private Color baseColour = Color.white ;

  private LaserWeapon weapon ;

  private void Awake () {
    weapon = GetComponent< LaserWeapon >() ;
  }

  public void Update () {
    material.color = Color.Lerp( baseColour, chargeColour, weapon.CurrentCharge ) ;
  }

}
