using UnityEngine.EventSystems ;
using UnityEngine ;

//
// Test using Unity's new eventsystem
public class DestructionEventData< T > : BaseEventData {

  //
  // Static - Method to make calling the event easier
  private readonly static ExecuteEvents.EventFunction<IDestructionHandler <T>> CAST_DELEGATE
    = delegate(IDestructionHandler<T> handler, BaseEventData data) {
      var cast = ExecuteEvents.ValidateEventData<DestructionEventData< T >>( data ) ;
      handler.OnDestruction( cast ) ;
    };

  //
  // Instance
  public T target ;

  public DestructionEventData(EventSystem eventSystem, T target) : base( eventSystem ) {
    this.target = target ;
  }

  public void Cast(GameObject handler) {
    ExecuteEvents.ExecuteHierarchy<IDestructionHandler< T >>(handler, this, CAST_DELEGATE);
  }

}
