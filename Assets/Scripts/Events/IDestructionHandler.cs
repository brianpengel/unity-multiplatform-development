using UnityEngine.EventSystems ;

public interface IDestructionHandler <T> : IEventSystemHandler {

  void OnDestruction (DestructionEventData< T > e) ;

}
