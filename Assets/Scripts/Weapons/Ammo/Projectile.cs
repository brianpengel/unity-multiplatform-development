using System.Collections.Generic ;
using System.Collections ;
using UnityEngine ;

public class Projectile : ObjectPool< Projectile >.PoolableObject {

  [SerializeField] private float speed ;
  private Vector3 prevPos ;
  private float bulletDamage ;

  public void Init (Vector3 position, Quaternion rotation, float damage) {
    transform.position = position ;
    transform.rotation = rotation ;
    gameObject.SetActive( true ) ;
    bulletDamage = damage ;
    Step() ;
  }

  void Update () {
    CheckCollision() ;
    Step() ;
  }

  private void Step () {
    prevPos = transform.position ;
    transform.position = transform.position + (transform.forward * speed * Time.deltaTime) ;
  }

  private void CheckCollision () {
    Ray ray = new Ray( prevPos, transform.position ) ;
    RaycastHit hit ;

    if(Physics.Raycast( ray, out hit )) {
      GameObject other = hit.collider.gameObject ;
      IDamagable damagable = other.GetComponent< IDamagable > () ;

      if( damagable != null )
        damagable.ReceiveDamage( bulletDamage ) ;

      gameObject.SetActive( false ) ;
    }
  }

  private void OnBecameInvisible() {
    gameObject.SetActive( false ) ;
  }
}
