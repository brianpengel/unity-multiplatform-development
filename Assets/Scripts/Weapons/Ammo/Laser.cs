using UnityEngine ;

[RequireComponent( typeof( LineRenderer ) )]
public class Laser : MonoBehaviour {

  [SerializeField] private AnimationCurve curve = AnimationCurve.Linear(0, 0, 1, 1) ;
  [SerializeField] private float laserMaxWidth = .1f ;
  private LineRenderer line ;

  private void Awake () {
    line = GetComponent< LineRenderer > () ;
  }

  public void Cast (float t, Vector3 origin, Quaternion rotation, float maxDistance, float damagePerTick) {
    Vector3 dir = rotation * Vector3.forward ;
    Debug.DrawLine(origin, origin + dir * maxDistance) ;

    line.widthMultiplier = Ease(t) * laserMaxWidth ;
    line.SetPosition( 0, origin ) ;


    Ray ray = new Ray( origin, dir ) ;
    RaycastHit hit ;

    if(!Physics.SphereCast( ray, line.widthMultiplier, out hit, maxDistance )) {
      line.SetPosition( 1, ray.GetPoint( maxDistance ) ) ;
      return ;
    }

    line.SetPosition( 1, hit.point ) ;

    GameObject other = hit.collider.gameObject ;
    IDamagable damagable = other.GetComponent< IDamagable > () ;

    if( damagable != null )
      damagable.ReceiveDamage( damagePerTick ) ;
  }

  private float Ease (float t) {
    return t * ( 2 - t ) ;
  }

}
