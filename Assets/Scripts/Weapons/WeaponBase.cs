using UnityEngine ;

public abstract class WeaponBase : MonoBehaviour {

  [SerializeField] protected float fireDelay = .2f ;
  [SerializeField] protected float damage = .1f ;
  [SerializeField] protected float drag = 0 ;

  public float Drag {
    set { drag = value ; }
    get { return drag ; }
  }

  public abstract void Fire () ;

}
