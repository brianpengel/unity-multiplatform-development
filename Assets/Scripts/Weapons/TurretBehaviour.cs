using UnityEngine ;

public class TurretBehaviour : MonoBehaviour {

  [SerializeField] private Transform weaponPivot ;

  [SerializeField] private WeaponBase[] availableWeapons ;
  [SerializeField] private WeaponBase currentWeapon ;
  [SerializeField] private float turnSpeed = 9 ;
  private int currentWeaponIndex ;

  public WeaponBase CurrentWeapon {
    set { currentWeapon = value ; }
    get { return currentWeapon ; }
  }

  //
  // Unity Events
  private void Awake () {
    foreach( var weapon in availableWeapons )
      weapon.gameObject.SetActive( false ) ;

    if( CurrentWeapon == null && availableWeapons.Length > 0 )
      CurrentWeapon = availableWeapons[ currentWeaponIndex ] ;

    if( CurrentWeapon != null )
      CurrentWeapon.gameObject.SetActive( true ) ;
  }

  //
  // Shoot / Fire weapon
  public void FireWeapon () {
    if( CurrentWeapon != null )
      CurrentWeapon.Fire() ;
  }

  //
  // Weapon Cycling / Swapping
  public void CycleWeapons ( bool inverse = false ) {
    if( availableWeapons.Length == 0 )
      return ;

    if( inverse )
         currentWeaponIndex = currentWeaponIndex - 1 >= 0 ? currentWeaponIndex - 1 : availableWeapons.Length - 1 ;
    else currentWeaponIndex = ( currentWeaponIndex + 1 ) % availableWeapons.Length ;

    CurrentWeapon.gameObject.SetActive( false ) ;
    CurrentWeapon = availableWeapons[ currentWeaponIndex ] ;
    CurrentWeapon.gameObject.SetActive( true ) ;
  }

  //
  // Rotate weapon in direction
  public void LookAt (Vector3 direction) {
    // Reset Y of direction to prevent vertical rotation
    direction[ 1 ] = 0 ;

    float weaponDrag = 1f - (currentWeapon == null ? 0 : currentWeapon.Drag) ;
    var targetRotation = Quaternion.LookRotation( direction ) ;
    var rotationStep = Time.deltaTime * turnSpeed * weaponDrag ;

    weaponPivot.rotation = Quaternion.Lerp( weaponPivot.rotation, targetRotation, rotationStep ) ;
  }

}
