﻿using UnityEngine;

public class ProjectileWeapon : WeaponBase {

  [SerializeField] protected Transform [] muzzles ;
  [SerializeField] private Projectile projectiles ;

  private float lastFired ;
  private int muzzleIndex ;

  public override void Fire () {
    if( Time.time - lastFired >= fireDelay && projectiles != null ) {
      projectiles.Pool.Get().Init(
        muzzles[ muzzleIndex ].position,
        muzzles[ muzzleIndex ].rotation,
        damage
      ) ;

      muzzleIndex = (muzzleIndex + 1) % muzzles.Length ;
      lastFired = Time.time ;
    }
  }

}
