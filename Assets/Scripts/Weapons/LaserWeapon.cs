﻿using System.Collections.Generic ;
﻿using System.Collections ;
﻿using UnityEngine ;

public class LaserWeapon : WeaponBase {

  [SerializeField] private Transform muzzle ;
  [SerializeField] private Laser laser ;
  [SerializeField] private float laserLifetime = 2f ;

  [SerializeField] private float chargeDrag = .5f ;
  [SerializeField] private float firingDrag = .9f ;
  private float regularDrag ;

  [SerializeField] private float chargeDecayMultiplier = .1f ;
  [SerializeField] private float chargeTime = 2f ;
  [SerializeField] private float range = 2f ;

  private Coroutine stateRoutine ;
  private int lastInput = -1 ;

  //
  //
  public float CurrentCharge { get; private set ; }

  //
  // Events
  private void OnEnable () {
    regularDrag = drag ;
    stateRoutine = StartCoroutine ( StateLoop() ) ;
  }

  private void OnDisable () {
    laser.gameObject.SetActive( false ) ;
    CurrentCharge = 0 ;
    drag = regularDrag ;

    if( stateRoutine != null ) {
      StopCoroutine( stateRoutine ) ;
      stateRoutine = null ;
    }
  }

  //
  //
  public override void Fire () {
    lastInput = Time.frameCount ;
  }

  //
  // Laser States
  private IEnumerator StateLoop () {
    while( true ) {
      yield return ChargeState() ;
      yield return FiringState() ;
      yield return CooldownState() ;
    }
  }

  //
  // Charging
  private IEnumerator ChargeState () {
    float t = 0 ;

    while( t <= chargeTime ) {
      yield return null ;

      if( lastInput == Time.frameCount ) {
        drag = chargeDrag ;
        t += Time.deltaTime ;
      }

      else if ( t > 0 )
        t = Mathf.Max(0, t - Time.deltaTime * chargeDecayMultiplier) ;

      CurrentCharge = 1f / chargeTime * t ;
    }

    drag = regularDrag ;
  }

  //
  // Firing
  private IEnumerator FiringState () {
    float lifetime = 0 ;
    drag = firingDrag ;

    laser.gameObject.SetActive( true ) ;

    while ( lifetime < laserLifetime ) {
      float t = 1f / laserLifetime * lifetime ;
      laser.Cast( 1f - t, muzzle.position, transform.rotation, range, damage ) ;
      yield return null ;
      lifetime += Time.deltaTime ;
    }

    drag = regularDrag ;
    laser.gameObject.SetActive( false ) ;
  }

  //
  // Cooling down
  private IEnumerator CooldownState () {
    float t = 0 ;

    while ( t < fireDelay ) {
      t += Time.deltaTime ;
      CurrentCharge = 1f - 1f / fireDelay * t ;
      yield return null ;
    }
  }

}
