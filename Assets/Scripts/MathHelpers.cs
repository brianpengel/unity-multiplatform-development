using UnityEngine ;

public static class MathHelpers {

  public static Vector2 PointOnCircleDeg (float degrees, float radius = 1, Vector2 origin = default(Vector2)) {
    var angle = Mathf.Deg2Rad * degrees ;
    return PointOnCircle(angle, radius, origin) ;
  }

  public static Vector2 PointOnCirclePercentage (float t, float radius = 1, Vector2 origin = default(Vector2)) {
    return PointOnCircleDeg(t * 360, radius, origin) ;
  }

  public static Vector2 PointOnCircle (float radians, float radius = 1, Vector2 origin = default(Vector2)) {
    return new Vector2 (
      origin.x + radius * Mathf.Cos(radians),
      origin.y + radius * Mathf.Sin(radians)
    );
  }

}
