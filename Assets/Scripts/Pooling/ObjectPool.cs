using System.Collections.Generic ;
using UnityEngine ;

// Simple objectpool
public abstract partial class ObjectPool< T > : MonoBehaviour where T : ObjectPool< T >.PoolableObject {

  //
  // STATICS
  public static ObjectPool< T > Instance { get ; private set ; }

  //
  // Instance
  [SerializeField] private T objectToPool ;
  [SerializeField] private int initialPoolSize = 50 ;
  private List<T> pooledItems ;

  //
  //
  private void Awake () {
    if ( Instance != null )
         Destroy( gameObject ) ;
    else Instance = this ;

    pooledItems = new List< T >( ) ;
    Extend( initialPoolSize ) ;
  }

  //
  // Factory
  protected virtual T CreateItem () {
    var obj = Instantiate( objectToPool ) ;
    obj.gameObject.name = objectToPool.name + " pool" ;
    obj.gameObject.SetActive( false ) ;
    obj.transform.SetParent( transform ) ;
    return obj ;
  }

  //
  //
  public void Extend ( int amount ) {
    for( int i = 0 ; i < amount ; i ++ )
      pooledItems.Add( CreateItem () ) ;
  }

  //
  // TODO - Optimize lookup
  public T Get () {
    for (int i = 0; i < pooledItems.Count; i++)
      if (!pooledItems[i].gameObject.activeInHierarchy)
        return pooledItems[i];

    var created = CreateItem () ;
    pooledItems.Add( created ) ;
    return created ;
  }
}
