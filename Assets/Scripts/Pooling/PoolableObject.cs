﻿using UnityEngine;

public abstract partial class ObjectPool< T > {
  // Nested class seperated for readabillity
  public class PoolableObject : MonoBehaviour {

    public ObjectPool< T > Pool {
      get { return ObjectPool< T >.Instance ; }
    }

  }
}
