using System.Collections.Generic;
﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class MenuView : MonoBehaviour {

  [SerializeField] private InputHandler[] controllers ;
  [SerializeField] private Dropdown selectionDropdown ;
  [SerializeField] private Text descriptionContainer ;
  [SerializeField] private Button startButton ;
  private List<InputHandler> availableControllers ;

  private void Awake () {
    UpdateOptions() ;
    UpdateDescription() ;

    selectionDropdown.onValueChanged.AddListener(i=>UpdateDescription()) ;
    startButton.onClick.AddListener(StartGame) ;
  }

  private void UpdateOptions () {
    availableControllers = new List<InputHandler> () ;
    var options = new List<string> () ;

    foreach(var c in controllers)
      if(c.isValid()) {
        availableControllers.Add( c ) ;
        options.Add( c.Title ) ;
      }

    selectionDropdown.ClearOptions();
    selectionDropdown.AddOptions(options);
    selectionDropdown.RefreshShownValue() ;
  }

  private void UpdateDescription () {
    if(descriptionContainer != null && availableControllers.Count > 0)
      descriptionContainer.text = availableControllers[selectionDropdown.value].Description ;
  }

  private void StartGame () {
    ControllerAplicator.persistentController = availableControllers.Count > 0 ?
      availableControllers[selectionDropdown.value] :
      null ;
      
    SceneManager.LoadScene(1) ;
  }

}
