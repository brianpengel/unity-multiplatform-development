using UnityEngine ;

[CreateAssetMenu]
public class MouseInputHandler : InputHandler {

  public override void Apply (TurretBehaviour turret) {
    // Aim
    var mousePos = Camera.main.ScreenToWorldPoint( Input.mousePosition ) ;
    var targetPos = mousePos - turret.transform.position ;

    turret.LookAt( targetPos ) ;

    // Change Weapons
    var scrollDelta = Input.mouseScrollDelta.y ;
    if( scrollDelta < 0 )
      turret.CycleWeapons( true ) ;

    else if( scrollDelta > 0 )
      turret.CycleWeapons() ;

    // Fire
    if ( Input.GetMouseButton( 0 ) )
      turret.FireWeapon () ;
  }

  public override bool isValid () {
    return Application.platform == RuntimePlatform.WindowsPlayer
        || Application.platform == RuntimePlatform.WindowsEditor
        || Application.platform == RuntimePlatform.OSXPlayer
        || Application.platform == RuntimePlatform.OSXEditor ;
  }

}
