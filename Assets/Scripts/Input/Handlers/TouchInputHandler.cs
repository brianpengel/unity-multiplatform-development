using UnityEngine.UI ;
using UnityEngine ;

[CreateAssetMenu]
public class TouchInputHandler : InputHandler, IRequireUI {

  [SerializeField] private Button cycleButtonAlt ;
  [SerializeField] private Button cycleButton ;
  private int cycleFrameAlt, cycleFrame ;

  public void SetupUI(Canvas canvas) {
    var btn = Instantiate( cycleButton ) ;
    btn.transform.SetParent( canvas.transform, false ) ;
    btn.onClick.AddListener(()=>cycleFrame = Time.frameCount) ;

    btn = Instantiate( cycleButtonAlt ) ;
    btn.transform.SetParent( canvas.transform, false ) ;
    btn.onClick.AddListener(()=>cycleFrameAlt = Time.frameCount) ;
  }

  public override void Apply (TurretBehaviour turret) {
    if (Input.touchCount > 0) {
      var touchPos = Camera.main.ScreenToWorldPoint( Input.GetTouch(0).position ) ;
      var targetPos = touchPos - turret.transform.position ;

      turret.LookAt( targetPos ) ;
      turret.FireWeapon() ;
    }

    if( cycleFrame == Time.frameCount )
      turret.CycleWeapons() ;

    else if( cycleFrameAlt == Time.frameCount )
      turret.CycleWeapons( true ) ;
  }

  public override bool isValid () {
    return Application.platform == RuntimePlatform.Android
        || Application.platform == RuntimePlatform.IPhonePlayer
        || Application.platform == RuntimePlatform.WindowsEditor ;
  }
}
