using UnityEngine ;

[CreateAssetMenu]
public class KeyboardInputHandler : InputHandler {

  // Directional keys
  [SerializeField] private KeyCode UpKey    = KeyCode.W ;
  [SerializeField] private KeyCode leftKey  = KeyCode.A ;
  [SerializeField] private KeyCode downKey  = KeyCode.S ;
  [SerializeField] private KeyCode rightKey = KeyCode.D ;

  [SerializeField] private KeyCode fireKey  = KeyCode.Space ;

  [SerializeField] private KeyCode weaponCycleKeyAlt = KeyCode.Q ;
  [SerializeField] private KeyCode weaponCycleKey    = KeyCode.E ;

  public override void Apply (TurretBehaviour turret) {
    int u = Input.GetKey( UpKey ) ? 0 : 1 ;
    int d = Input.GetKey( downKey ) ? 0 : 1 ;
    int l = Input.GetKey( leftKey ) ? 0 : 1 ;
    int r = Input.GetKey( rightKey ) ? 0 : 1 ;

    var dir = new Vector3 (l - r, 0, d - u) ;

    if( dir != Vector3.zero )
      turret.LookAt( dir ) ;

    if ( Input.GetKeyDown( weaponCycleKeyAlt ) )
      turret.CycleWeapons ( true ) ;

    else if ( Input.GetKeyDown( weaponCycleKey ) )
      turret.CycleWeapons () ;

    // Fire
    if ( Input.GetKey( fireKey ) )
      turret.FireWeapon () ;

  }

  public override bool isValid () {
    return Application.platform == RuntimePlatform.WindowsPlayer
        || Application.platform == RuntimePlatform.WindowsEditor
        || Application.platform == RuntimePlatform.OSXPlayer
        || Application.platform == RuntimePlatform.OSXEditor ;
  }

}
