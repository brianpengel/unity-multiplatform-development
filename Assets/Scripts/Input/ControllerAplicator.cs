﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerAplicator : MonoBehaviour {

  public static InputHandler persistentController ;

  [SerializeField] private TurretBehaviour target ;
  [SerializeField] private InputHandler fallback ;
  [SerializeField] private Canvas uiParent ;

  [SerializeField] private InputHandler controller ;

  private void Awake () {
    controller = persistentController == null ? fallback : persistentController ;

    if( controller is IRequireUI )
      ((IRequireUI)controller).SetupUI( uiParent ) ;
  }

  private void Update () {
    if( controller != null )
      controller.Apply ( target ) ;
  }
}
