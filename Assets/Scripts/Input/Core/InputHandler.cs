using UnityEngine ;

public abstract class InputHandler : ScriptableObject, IDescribable {

  [SerializeField] private string title ;
  [SerializeField, TextArea] private string description ;

  public string Title       { get { return title ; } }
  public string Description { get { return description ; } }

  public abstract void Apply (TurretBehaviour turret) ;
  public abstract bool isValid () ;

}
