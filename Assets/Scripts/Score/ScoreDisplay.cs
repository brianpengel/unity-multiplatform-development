﻿using UnityEngine.UI;
﻿using UnityEngine;

[RequireComponent( typeof(Text) )]
public class ScoreDisplay : MonoBehaviour {

  [SerializeField] private ScoreContainer container ;
  private Text display ;

  private void Awake () {
    display = GetComponent< Text > () ;
  }

  private void Update () {
    display.text = container.Score.ToString() ;
  }

}
