﻿﻿using UnityEngine;

public class ScoreContainer : MonoBehaviour, IDestructionHandler <EnemyBase> {

  [SerializeField] private int score = 0 ;

  public int Score { get { return score ; } }

  public void OnDestruction (DestructionEventData< EnemyBase > e) {
    score += e.target.Value ;
  }

}
