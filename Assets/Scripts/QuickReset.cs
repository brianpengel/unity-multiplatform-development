using UnityEngine.SceneManagement;
using UnityEngine;

//
// Class For Demo's
// Will reset game by loading scene 0 on Key( resetKey )
public class QuickReset : MonoBehaviour {

  [SerializeField] private KeyCode resetKey = KeyCode.Escape ;

  void Update() {
    if(Input.GetKeyDown( resetKey ))
      SceneManager.LoadScene(0) ;
  }
}
