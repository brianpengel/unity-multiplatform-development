using System.Collections.Generic;
﻿using System.Collections;
using UnityEngine.EventSystems ;
using UnityEngine ;

public class EnemyBase : ObjectPool< EnemyBase >.PoolableObject, IDamagable {

  // Quaternions can't be constant
  private readonly static Quaternion CIRCLE_ROTATION = Quaternion.Euler( 90, 0, 0 ) ;

  //
  // Instance
  [SerializeField] protected float circleSpeed = 2 ;
  [SerializeField] protected int hitpoints = 1 ;
  [SerializeField] protected int value = 1 ;

  private float currentHitpoints ;
  private Coroutine state ;


  public int Hitpoints {
    get { return Mathf.RoundToInt(this.currentHitpoints) ; }
  }

  public int Value {
    get { return this.value ; }
  }


  public void Init (float t, float dist) {
    gameObject.SetActive( true ) ;
    state = StartCoroutine(StateLoop(t, dist)) ;
    currentHitpoints = hitpoints ;
  }

  private void OnDisable () {
    if( state != null )
      StopCoroutine( state ) ;
  }

  //
  // State Behaviour
  private IEnumerator StateLoop (float t, float dist) {
    while( gameObject.activeSelf ) {
      var pos = CIRCLE_ROTATION * MathHelpers.PointOnCirclePercentage(t, dist + Mathf.Sin(Time.time)) ;
      transform.position = pos + Vector3.up * transform.position.y ;

      yield return null ;
      t += Time.deltaTime * circleSpeed ;
    }
  }

  //
  // IDamagable.ReceiveDamage
  public void ReceiveDamage (float damage) {
    currentHitpoints -= damage ;

    if( currentHitpoints <= 0  ) {
      // Update score
      var e = new DestructionEventData< EnemyBase >(EventSystem.current, this) ;
      e.Cast( gameObject ) ;
      gameObject.SetActive( false ) ;
    }
  }

}
