using System.Collections.Generic;
﻿using System.Collections;
using UnityEngine;

public class WaveManager : MonoBehaviour, IDestructionHandler<EnemyBase> {

  [SerializeField] private EnemyBase enemyPrefab ;
  [SerializeField] private float spawnRange = 5 ;
  [SerializeField] private int maxEnemies = 6 ;
  [SerializeField] private int minEnemies = 2 ;

  private int activeEnemies = 0 ;


  private void Update () {
    if( activeEnemies == 0 )
      SpawnWave() ;
  }

  private void OnDrawGizmosSelected () {
    Gizmos.color = Color.yellow;
    Gizmos.DrawWireSphere( transform.position, spawnRange );
  }

  private void SpawnWave () {
    int amount = Random.Range( minEnemies, maxEnemies ) ;
    float step = 1f / amount ;

    for(int i = 0 ; i < amount ; ++ i)
      enemyPrefab.Pool.Get().Init(i * step, spawnRange) ;

    activeEnemies = amount ;
  }

  //
  // IDestructionHandler<EnemyBase>.OnDestruction
  public void OnDestruction (DestructionEventData< EnemyBase > e) {
    -- activeEnemies ;
  }

}
