using UnityEngine ;

[CreateAssetMenu]
public class AIInputHandler : InputHandler {

  [SerializeField] private LayerMask targetLayer ;
  [SerializeField] private float updateDelay = 1 ;
  [SerializeField] private float fov = 15 ;
  [SerializeField] private float range = 4 ;

  private EnemyBase target = null ;
  private float lastUpdate = 0 ;

  //
  // Unity events
  public void OnEnable() {
    lastUpdate = 0 ;
    target = null ;
  }

  public void OnDisable() {
    OnEnable() ;
  }

  //
  // Instance
  private void GetTarget (TurretBehaviour turret) {
    var colliders = Physics.OverlapSphere(turret.transform.position, range, targetLayer);
    target = colliders.Length == 0 ? null : colliders[0].GetComponent< EnemyBase > () ;
    lastUpdate = Time.time ;
  }

  //
  // InputHandler
  public override void Apply (TurretBehaviour turret) {
    if( target == null && Time.time - lastUpdate > updateDelay )
      GetTarget(turret) ;

    // Shoot at target
    if( target != null ) {
      var dir = (target.transform.position - turret.transform.position).normalized ;
      var angle = Vector3.Angle(dir, turret.CurrentWeapon.transform.forward) ;

      turret.LookAt( dir ) ;

      if( angle < fov )
        turret.FireWeapon() ;

      if(!target.gameObject.activeSelf || dir.magnitude > range)
        target = null ;
    }
  }

  public override bool isValid () {
    return true ;
  }

}
